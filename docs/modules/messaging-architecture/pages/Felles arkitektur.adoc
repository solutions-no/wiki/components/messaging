= Felles arkitektur
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: main@messaging:messaging-architecture:
endif::[]
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 0

Arkitektur for meldingsutveksling....

:leveloffset: +1
include::main@messaging:messaging-architecture:page$Kapabiliteter.adoc[]
include::main@messaging:messaging-architecture:page$Forretningstjenester.adoc[]
include::main@messaging:messaging-architecture:page$Applikasjonstjenester.adoc[]
include::main@messaging:messaging-architecture:page$Osv..adoc[]
:leveloffset: -1
