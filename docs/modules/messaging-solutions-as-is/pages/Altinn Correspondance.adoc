= Altinn Correspondance
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: main@messaging:messaging-solutions-as-is:
endif::[]
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 0



.Altinn Correspondance
image::{imagepath}Altinn Correspondance.png[alt=Altinn Correspondance image, link=https://solutions-no.github.io/models/archi/?view=id-464056c5c4874dfcacbd7a3797d2c0da]


****
xref:main@messaging:messaging-solutions-as-is:page$Altinn Correspondance.var.1.adoc[Vis detaljer om elementene i diagrammet] (Tips: kbd:[Shift]-klikk for å åpne i nytt vindu)
****


