ifndef::difi_orig_language[]
:difi_orig_language: {lang}
endif::difi_orig_language[]
//
//
//:doctype: book
ifeval::["{lang}" == "no"]
//************************
ifdef::doctitle_no[]
:doctitle: {doctitle_no}
endif::doctitle_no[]
//
////
ifndef::author[]
ifndef::author_no[]
:author_no: Direktoratet for forvaltning og ikt (Difi)
endif::author_no[]
ifdef::author_no[]
:author: {author_no}
endif::author_no[]
endif::author[]
////
//
ifdef::keywords_no[]
:keywords: {keywords_no}
endif::keywords_no[]
//**********************
endif::[]
//
ifeval::["{lang}" == "en"]
//************************
ifdef::doctitle_en[]
:doctitle: {doctitle_en}
endif::doctitle_en[]
////
ifndef::author[]
ifndef::author_en[]
:author_en: Agency for Public Management and eGovernment (Difi)
endif::author_en[]
ifdef::author_en[]
:author: {author_en}
endif::[]
endif::author[]
////
//
ifdef::keywords_en[]
:keywords: {keywords_en}
endif::keywords_en[]
//**********************
endif::[]
//
// Output evaluated text:
////
ifdef::doctitle[]
== {doctitle}
endif::[]
////
////
//:output_dificonvention_errors:
ifdef::output_dificonvention_errors[]
ifndef::doctitle[]
[red small]__Difi convention error (standardheader.adoc): Missing "doctitle"__
endif::doctitle[]
endif::output_dificonvention_errors[]
////
//
////
ifdef::author[]
{author}
endif::[]
ifndef::email[]
:email: nasjonalarkitektur@digdir.no
endif::[]
ifdef::revdate[]
{revdate}
endif::[]
ifndef::revdate[]
endif::[]
////
//
//:toc-title: {toc-header}
:icons: font
//:docinfodir: ../common/meta
//:docinfo:
//:toc: macro
:leveloffset: +0
//ifndef::toclevels[]
//:toc:
:sectnums:
:toclevels: 7
:sectnumlevels: 9
//endif::toclevels[]
:sectlinks:
:linkattrs:
:sectids:
:sectanchors:

:xrefstyle: short

ifeval::["{lang}" == "no"]
:note-caption: Info
:tip-caption: Tips
:warning-caption: Advarsel
:important-caption: Viktig
:caution-caption: OBS!
endif::[]

// test...
:if_no: ifeval::["{lang}" == "no"]

// :imagesdir!:

:imagesdir: ../images
//:imagepath: ../images
//:imagepath: {modulepath}



//:imagesdir@: ./media
////
ifdef::attr-root-dir[]
:imagesdir@: ./common/media
endif::[]
////
// data-uri benyttes for å inkludere images i generert html-fil :data-uri:

// Erik, 2020-02-05 Skip toc since not needed with Antora
//:toc: left

// consider deprecate support for preamble and rely on .lead instead
ifdef::preamble[]
{preamble}
endif::[]



////
ifeval::["{toc}" == "macro"]
toc::[]
endif::[]
////

//image:difilogo.svg[image,width=169,height=66]

//:difi_include_implementation_notes:
ifdef::difi_include_implementation_notes[]
= TIP: Implementation note, Erik, 2018-08-18: Consider introducing attributes to indicate the state of the translation and give hints to the user about that: difi_orig_language (normally  _no_ or _en_) and difi_translation_status (set to _complete_, _partial_ or _missing_).
endif::difi_include_implementation_notes[]


//:difi_test_translation_status:
////
ifdef::difi_test_translation_status[]
:lang: en
:difi_orig_language: no
:difi_translation_status: incomplete
endif::difi_test_translation_status[]
////

ifndef::difi_translation_status[]
:difi_translation_status: -
endif::difi_translation_status[]
ifeval::["{difi_translation_status}" == ""]
:difi_translation_status: -
endif::[]
ifeval::["{difi_translation_status}" == "-"]
:difi_translation_status: missing
endif::[]

////
ifdef::difi_doctype[]
ifeval::["{lang}" == "en"]
_Document type: {difi_doctype}_
endif::[]
ifeval::["{lang}" == "no"]
_{difi-contenttype-caption}: {difi_doctype}_
endif::[]
endif::difi_doctype[]
////

ifdef::difi_translation_status[]
// English
ifeval::["{lang}" == "en"]
ifeval::["{difi_orig_language}" != "en"]
ifeval::["{difi_translation_status}" != "OK"]
NOTE: This document was originally written in another language ({difi_orig_language}).
The English translation is marked as __{difi_translation_status}__
.
endif::[]
endif::[]
endif::[]
//
// Norwegian
ifeval::["{lang}" == "no"]
ifeval::["{difi_orig_language}" != "no"]
ifeval::["{difi_translation_status}" != "OK"]

NOTE: Dette dokumentet ble opprinnelig skrevet på et annet språk ({difi_orig_language}). Norsk oversettelse er markert som __{difi_translation_status}__
ifeval::["{difi_translation_status}" == "missing"]
(__ikke oversatt__)
endif::[]
ifeval::["{difi_translation_status}" == "incomplete"]
(__uferdig__)
endif::[]
.
endif::[]
endif::[]
endif::[]
//
endif::difi_translation_status[]


// https://asciidoctor.org/docs/user-manual/#putting-images-in-their-place
ifeval::[{wysiwig_editing} == 1]
:under-construction-image: image::under-construction.png[width=40,float="left"]
endif::[]
ifeval::[{wysiwig_editing} == 0]
:under-construction-image: image:architecture-repository:common:under-construction.png[width=40]
endif::[]