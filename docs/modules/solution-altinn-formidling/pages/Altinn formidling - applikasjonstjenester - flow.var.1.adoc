= Altinn formidling - applikasjonstjenester - flow
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: main@messaging:solution-altinn-formidling:
endif::[]
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 0



.Altinn formidling - applikasjonstjenester - flow
image::{imagepath}Altinn formidling - applikasjonstjenester - flow.png[alt=Altinn formidling - applikasjonstjenester - flow image, link=https://altinn.github.io/ark/models/archi-all?view=id-e6d5b64ece904784aa06ed3b80f711f6]


TIP: Gå tilbake til standardvisning ved å klikke 'tilbake' (eller lukk dette vinduet om det er nytt)


[cols ="1,3", options="header"]
.Elementer i view for Altinn formidling - applikasjonstjenester - flow
|===

| Element
| Beskrivelse

| Avsender 
a| Den som sender en elektronisk melding eller tilsvarende.

| Mottaker
a| Den som mottar en elektronisk melding eller tilsvarende.

| Altinn Formidling
a| 

| Check  if available Files
a| Lettvekt metode for mottakere som ofte må sjekke om det er nye filer tilgjengelig.
Sjekker kun i db, hopper over autorisasjons-steg. Returnerer True/false.


| Get Available Files
a| Henter en "avgivers" (Erik: du mener mottakers?) tilgjengelige formidlingstjenester med metadata og referanse for nedlasting. Inkluderer autorisasjon og henter ut liste med metadata for tilgjengelige filer.


| Download File
a| Strømmer ned fil for formidlingstjeneste basert på referanse. Autoriserer, strømmer ned fil, endrer IKKE status.

| Confirm downloaded
a| Bekrefter at mottaker har lastet ned filen, hvorpå filen kan saneres dersom den møter kriteriene.
Endrer FileRecipientStatus til "Downloaded".

| Get receipt confirmation
a| Hent ut kvittering knyttet til filen basert på FileReference.

| Update receipt confirmation
a| Mottaker: Oppdater tekst i mottakers kvittering, som er synlig også for avsender.
Avsender: Kan (teoretisk) også brukes til å oppdatere kvitteringer for avsender, men kun avsender kan se sin oppdaterte kvittering.

| Notify receipient
a| Brukes av Politiet til å sende en melding via Altinn Corresponance) til mottaker med nedlastningslenke.

| Get File Metadata
a| Hent fil metadata.​


| Upload file
a| Strømmer opp fil for en gitt FileReference basert på opprettelse av formidlingstjeneste.
Autoriserer, strømmer opp fil til sharet, oppdaterer manifest, utfører viruskontroll og setter status til "Uploaded" for både FileStatus og RecipientStatus.
Returnerer Receipt.


| Initiate message transfer
a| Forbereder opprettelse av en formidlingstjeneste ved å sende inn nødvendig metadata, hvorpå fil kan strømmes opp .Returnerer en FileReference, en GUID som brukes videre som identifikator.
Autoriserer og oppretter metadata i databasen, FileStatus=Initialized


|===
****
TIP: Gå tilbake til standardvisning ved å klikke 'tilbake' (eller lukk dette vinduet om det er nytt)
****


