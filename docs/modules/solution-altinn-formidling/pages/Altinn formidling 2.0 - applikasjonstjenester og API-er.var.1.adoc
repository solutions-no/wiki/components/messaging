= Altinn formidling 2.0 - applikasjonstjenester og API-er
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: main@messaging:solution-altinn-formidling:
endif::[]
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 0



.Altinn formidling 2.0 - applikasjonstjenester og API-er
image::{imagepath}Altinn formidling 2.0 - applikasjonstjenester og API-er.png[alt=Altinn formidling 2.0 - applikasjonstjenester og API-er image, link=https://altinn.github.io/ark/models/archi-all?view=id-07d8cdb410ca4bda9d2a15c59fe0275b]


TIP: Gå tilbake til standardvisning ved å klikke 'tilbake' (eller lukk dette vinduet om det er nytt)


[cols ="1,3", options="header"]
.Elementer i view for Altinn formidling 2.0 - applikasjonstjenester og API-er
|===

| Element
| Beskrivelse

| Avsender 
a| Den som sender en elektronisk melding eller tilsvarende.

| Mottaker
a| Den som mottar en elektronisk melding eller tilsvarende.

| Personbruker
a| 

| Altinn Formidling
a| 

| Check  if available Files
a| Lettvekt metode for mottakere som ofte må sjekke om det er nye filer tilgjengelig.
Sjekker kun i db, hopper over autorisasjons-steg. Returnerer True/false.


| Notify receipient
a| Brukes av Politiet til å sende en melding via Altinn Corresponance) til mottaker med nedlastningslenke.

| Update receipt confirmation
a| Mottaker: Oppdater tekst i mottakers kvittering, som er synlig også for avsender.
Avsender: Kan (teoretisk) også brukes til å oppdatere kvitteringer for avsender, men kun avsender kan se sin oppdaterte kvittering.

| Get receipt confirmation
a| Hent ut kvittering knyttet til filen basert på FileReference.

| Download File
a| Strømmer ned fil for formidlingstjeneste basert på referanse. Autoriserer, strømmer ned fil, endrer IKKE status.

| Initiate message transfer
a| Forbereder opprettelse av en formidlingstjeneste ved å sende inn nødvendig metadata, hvorpå fil kan strømmes opp .Returnerer en FileReference, en GUID som brukes videre som identifikator.
Autoriserer og oppretter metadata i databasen, FileStatus=Initialized


| Upload file
a| Strømmer opp fil for en gitt FileReference basert på opprettelse av formidlingstjeneste.
Autoriserer, strømmer opp fil til sharet, oppdaterer manifest, utfører viruskontroll og setter status til "Uploaded" for både FileStatus og RecipientStatus.
Returnerer Receipt.


| Get File Metadata
a| Hent fil metadata.​


| Get Available Files
a| Henter en "avgivers" (Erik: du mener mottakers?) tilgjengelige formidlingstjenester med metadata og referanse for nedlasting. Inkluderer autorisasjon og henter ut liste med metadata for tilgjengelige filer.


| Confirm downloaded
a| Bekrefter at mottaker har lastet ned filen, hvorpå filen kan saneres dersom den møter kriteriene.
Endrer FileRecipientStatus til "Downloaded".

| Altinn Formidling SOAP API
a| 

| BrokerService/ConfirmDownloaded
a| Bekrefter at mottaker har lastet ned filen, hvorpå filen kan saneres dersom den møter kriteriene.
Endrer FileRecipientStatus til "Downloadaed".

| BrokerServiceStreamed/DownloadFileStreamed
a| Strømmer ned fil for formidlingstjeneste basert på referanse. Autoriserer, strømmer ned fil, endrer IKKE status.


| BrokerService/CheckIfAvailableFiles
a| Lettvekt metode for mottakere som ofte må sjekke om det er nye filer tilgjengelig.
Sjekker kun i db, hopper over autorisasjons-steg. Returnerer True/false.


| BrokerService/GetAvailableFiles
a| Henter en avgivers tilgjengelige formidlingstjenester med metadata og referanse for nedlasting.Inkluderer autorisasjon og henter ut liste med metadata for tilgjengelige filer.

| Receipt/UpdateReceipt
a| Oppdater tekst i mottakers kvittering, som er synlig også for avsender.

| Receipt/GetReceiptV2
a| Hent ut kvittering knyttet til filen basert på FileReference.

| Altinn Formidling REST API
a| 

| GET https://www.altinn.no/api/my/brokerservice/files/{filereference}
a| URL som kan benyttes av personbrukere som er logget inn I Altinn-portalen til å laste ned fil. - Brukes av Politiet, implementert i 15.2.


| GET https://www.altinn.no/api/brokerservice/inbox/hasavailablefiles?serviceCode=myservicecode&serviceEditionCode=myserviceedition&recipients=mottaker1,mottaker
a| Lettvekt metode for mottakere som ofte må sjekke om det er nye filer tilgjengelig.Tilsvarer SOAP CheckIfAvailableFiles.

| GET https://www.altinn.no/api/{who}/brokerservice/inbox/?fileStatus=Uploaded&serviceCode=myservicecode&serviceEditionCode=myserviceedition&minSentDateTime=2021-01-01&maxSentDateTime=2021-12-01
a| Henter en avgivers tilgjengelige filer med metadata og referanse for nedlasting.
Tilsvarer SOAP GetAvailableFiles.


| GET https://www.altinn.no/api/{who}/brokerservice/inbox/{FileReference}
a| GET for å hente metadata for en spesifikk fil til mottaker.

| GET https://www.altinn.no/api/{who}/brokerservice/inbox/{FileReference}/receipt
a| GET for uthenting av fil receipt. Henter ut mottakers receipt. Vil kun vise receipt som tilhører mottaker.Tilsvarer SOAP GetReceiptV2, men avgrenset til formålet.

| POST https://www.altinn.no/api/{who}/brokerservice/inbox/{FileReference}/confirmdownloaded
a| POST for å manuelt bekrefte at en fil er lastet ned og mottatt.
Tilsvarer SOAP ConfirmDownloaded

| GET https://www.altinn.no/api/{who}/brokerservice/inbox/{FileReference}/download
a| GET for å laste ned fil via binary stream.
Tilsvarer SOAP DownloadFileStreamed.

| Altinn Formidling SOAP API
a| 

| Correspondence/InsertCorrespondenceV2
a| Brukes av Politiet til å sende en melding til mottaker med nedlastningslenke.

| BrokerService/InitiateBrokerService
a| Forbereder opprettelse av en formidlingstjeneste ved å sende inn nødvendig metadata, hvorpå fil kan strømmes opp .Returnerer en FileReference, en GUID som brukes videre som identifikator.
Autoriserer og oppretter metadata i databasen, FileStatus=Initialized

| BrokerServiceStreamed/UploadFileStreamed
a| Strømmer opp fil for en gitt FileReference basert på opprettelse av formidlingstjeneste.
Autoriserer, strømmer opp fil til sharet, oppdaterer manifest, utfører viruskontroll og setter status til "Uploaded" for både FileStatus og RecipientStatus.
Returnerer Receipt.


| Receipt/GetReceiptV2
a| Hent ut kvittering knyttet til filen basert på FileReference.

| Receipt/UpdateReceipt
a| Oppdater tekst i mottakers kvittering, som er synlig også for avsender.

| Altinn Formidling REST API
a| 

| POST https://www.altinn.no/api/{who}/brokerservice/outbox​
a| Opprett BrokerService Fil med metadata og binary stream.​

Tilsvarer SOAP sin InitiateBrokerService og UploadFileStreamed.

| GET https://www.altinn.no/api/{who}/brokerservice/outbox/{FileReference}
a| Hent fil metadata.​

| GET https://www.altinn.no/api/{who}/brokerservice/outbox/{FileReference}/receipt
a| Uthenting av fil kvittering /  Receipt​

Tilsvarer SOAP GetReceiptV2, men avgrenset til formål.​​

| Get file URL
a| URL som kan benyttes av personbrukere som er logget inn I Altinn-portalen til å laste ned fil. - Brukes av Politiet, implementert i 15.2.


|===
****
TIP: Gå tilbake til standardvisning ved å klikke 'tilbake' (eller lukk dette vinduet om det er nytt)
****


