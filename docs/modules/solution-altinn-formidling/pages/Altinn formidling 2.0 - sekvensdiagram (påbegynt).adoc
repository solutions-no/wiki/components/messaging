= Altinn formidling 2.0 - sekvensdiagram (påbegynt)
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: main@messaging:solution-altinn-formidling:
endif::[]
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 0



.Altinn formidling 2.0 - sekvensdiagram (påbegynt)
image::{imagepath}Altinn formidling 2.0 - sekvensdiagram (påbegynt).png[alt=Altinn formidling 2.0 - sekvensdiagram (påbegynt) image, link=https://altinn.github.io/ark/models/archi-all?view=id-d6362f6e46a34f1497cd6a5f0e40384c]


****
xref:main@messaging:solution-altinn-formidling:page$Altinn formidling 2.0 - sekvensdiagram (påbegynt).var.1.adoc[Vis detaljer om elementene i diagrammet] (Tips: kbd:[Shift]-klikk for å åpne i nytt vindu)
****


