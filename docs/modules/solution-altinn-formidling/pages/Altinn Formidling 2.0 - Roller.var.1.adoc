= Altinn Formidling 2.0 - Roller
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: main@messaging:solution-altinn-formidling:
endif::[]
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 0

Se også: https://solutions-no.github.io/models/?view=id-93f0fbf8f57a4b1485ba7da425bdbc7a[Archi view (Altinn Formidling 2.0 - Roller)]
 

.Altinn Formidling 2.0 - Roller
image::{imagepath}Altinn Formidling 2.0 - Roller.png[alt=Altinn Formidling 2.0 - Roller image, link=https://altinn.github.io/ark/models/archi-all?view=id-93f0fbf8f57a4b1485ba7da425bdbc7a]


TIP: Gå tilbake til standardvisning ved å klikke 'tilbake' (eller lukk dette vinduet om det er nytt)


[cols ="1,3", options="header"]
.Elementer i view for Altinn Formidling 2.0 - Roller
|===

| Element
| Beskrivelse

| Avsender 
a| Den som sender en elektronisk melding eller tilsvarende.

| Mottaker
a| Den som mottar en elektronisk melding eller tilsvarende.

| Tjenesteeier (Altinn)
a| 

| Sluttbruker (Altinn)
a| 

| Virksomhet
a| 

| Person
a| 

| Dataformidler
a| Dataformidler er et mellomledd som formidler data mellom produsent og konsument. Eksempler. Tilbyder av meldingsinfrastruktur, aksesspunkt, mellomliggende lagringsløsninger, sammenstilling av data fra flere datakilder.

Merk: Det er per 2019 uklart om rollen Dataformidler behøves, eller kan dekkes av rollen Daytatilbyder. Dette avhanger av pågående arbeid med juridiske spørsmåk (hvem eier data og hvem inngår avtaler med hvem).

| Personbruker
a| 

| Datatilbyder
a| Tilbyder av data til andre aktører, eventuelt på vegne av andre.

| Datakonsument
a| Den som innhenter eller mottar data fra andre aktører.

| Dataeier
a| 

| Maskinbruker
a| 

| Bruker
a| 

| Mottakeradministrasjon (eks. Politiet)
a| Vi bruker mottakeradministrasjon til å sende ut dokumenter digitalt til foretakene, som tidligere ble sendt i papir eller på minnepinne etc.

Jeg klarer ikke helt rydde i om dere alltid sender elektronisk, så sant det ikke er for store dokumenter eller gradert informasjon, eller om dere også har manuelle prosesser for å ivareta ikke digitale advokatpraksiser? 

Så langt det lar seg gjøre, så sendes dokumenter ut digitalt. Er det for mange dokumenter slik at det overskrider størrelsesbegrensningene i Altinn, så deler brukerne opp i flere forsendelser. Et gjentakende ønske fra brukerne våre er at denne grensen økes 

For de som ikke kan motta digitale forsendelser, så sendes dokumenter ut med gamle rutiner, dvs kopisak, minnepenn etc.

Er mottakeradministrasjon etablert fordi det ikke finnes noe felles register for digitale mottakere?

Mottakeradministrasjon hos oss ligger under en tjeneste som heter Straffesaksforsendelse og ble etablert for å kunne sende ut digitale dokumenter. Det er den eneste oversikten brukerne våre har over foretak det kan sendes digitale forsendelser til.

Foretaket legges inn ved hjelp av org.nr, og under foretaket kan brukerne legge inn personer som adressat, siden man ofte vet hvem man skal sende til, men ikke nødvendigvis hvilket advokatkontor.


|===
****
TIP: Gå tilbake til standardvisning ved å klikke 'tilbake' (eller lukk dette vinduet om det er nytt)
****


