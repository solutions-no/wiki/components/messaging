= Altinn formidling 2.0 - applikasjonstjenester og API-er
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: main@messaging:solution-altinn-formidling:
endif::[]
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 0



.Altinn formidling 2.0 - applikasjonstjenester og API-er
image::{imagepath}Altinn formidling 2.0 - applikasjonstjenester og API-er.png[alt=Altinn formidling 2.0 - applikasjonstjenester og API-er image, link=https://altinn.github.io/ark/models/archi-all?view=id-07d8cdb410ca4bda9d2a15c59fe0275b]


****
xref:main@messaging:solution-altinn-formidling:page$Altinn formidling 2.0 - applikasjonstjenester og API-er.var.1.adoc[Vis detaljer om elementene i diagrammet] (Tips: kbd:[Shift]-klikk for å åpne i nytt vindu)
****


