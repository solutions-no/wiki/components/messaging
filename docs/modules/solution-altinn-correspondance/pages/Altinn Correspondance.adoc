= Altinn Correspondance
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: main@messaging:solution-altinn-correspondance:
endif::[]
:toc: left
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 0

Om Altinn Correspondance...

:leveloffset: +1
include::main@messaging:solution-altinn-correspondance:page$Altinn Correspondance As-Is.adoc[]
include::main@messaging:solution-altinn-correspondance:page$Altinn Correspondance To-Be.adoc[]
:leveloffset: -1
