= Del og innhent data - overordnet aktørbilde
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: main@messaging:messaging-appendixes:
endif::[]
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 0



.Del og innhent data - overordnet aktørbilde
image::{imagepath}Del og innhent data - overordnet aktørbilde.png[alt=Del og innhent data - overordnet aktørbilde image, link=https://altinn.github.io/ark/models/archi-all?view=bfafce33-27f9-443c-8be7-83ebadbe4665]




