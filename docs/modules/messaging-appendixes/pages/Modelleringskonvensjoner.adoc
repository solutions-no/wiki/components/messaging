= Modelleringskonvensjoner
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: main@messaging:messaging-appendixes:
endif::[]
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 0



:leveloffset: +1
include::main@messaging:messaging-appendixes:page$Archimate-symboler.adoc[]
include::main@messaging:messaging-appendixes:page$Realisering.adoc[]
include::main@messaging:messaging-appendixes:page$Abstraksjon eksempler.adoc[]
include::main@messaging:messaging-appendixes:page$Abstraksjon - data.adoc[]
include::main@messaging:messaging-appendixes:page$Abstraksjon - tjeneste.adoc[]
include::main@messaging:messaging-appendixes:page$Abstraksjon - interface.adoc[]
include::main@messaging:messaging-appendixes:page$Abstraksjon - prosess.adoc[]
include::main@messaging:messaging-appendixes:page$Abstraksjon - ressurser.adoc[]
:leveloffset: -1
