= Martins oversikt (utkast)
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: main@messaging:messaging-appendixes:
endif::[]
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 0



.Martins oversikt (utkast)
image::{imagepath}Martins oversikt (utkast).png[alt=Martins oversikt (utkast) image, link=https://altinn.github.io/ark/models/archi-all?view=id-1f712d28b8b441caa9979facfaf01534]


****
xref:main@messaging:messaging-appendixes:page$Martins oversikt (utkast).var.1.adoc[Vis detaljer om elementene i diagrammet] (Tips: kbd:[Shift]-klikk for å åpne i nytt vindu)
****


