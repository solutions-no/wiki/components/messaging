= Tilrettelegg for datadeling
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: main@messaging:messaging-appendixes:
endif::[]
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 0



.Tilrettelegg for datadeling
image::{imagepath}Tilrettelegg for datadeling.png[alt=Tilrettelegg for datadeling image, link=https://altinn.github.io/ark/models/archi-all?view=fc8c4a98-f2a0-4795-8c54-c0a4fbddf814]


TIP: Gå tilbake til standardvisning ved å klikke 'tilbake' (eller lukk dette vinduet om det er nytt)


[cols ="1,3", options="header"]
.Elementer i view for Tilrettelegg for datadeling
|===

| Element
| Beskrivelse

| Tilrettelegg for dataoppdagelse og datadeling
a| 

| Publiser informasjon  om datasett, integrasjonsløsninger  og bruksvilkår
a| 

| Avklar juridiske og avtalemessige forhold for aktuelle datasett
a| Evnetuelt også innmelding i avtalefellesskap her

| Etabler standardløsninger for søk, integrasjon og tilgangskontroll
a| inkl. standardavtaler

| Etabler operative prosesser og ressurser for drift og feilhåndtering 
a| 

| Inngå avtaler med tiltrodde tredjeparter
a| 

| Utvikling og oppsett av tekniske løsninger
a| 

| Publisering av  informasjon om egne  tjenester og kapabiliteter
a| 

| Juridisk kompetanse og domenekompetanse
a| 

| Utvikling og oppset av løsninger for drift og forvaltning
a| 

| Avtaleforvaltning
a| 

| Mulig å oppdage og få tilgang til data
a| 

| Interessent
a| 

| Datatilbyder
a| Tilbyder av data til andre aktører, eventuelt på vegne av andre.

|===
****
TIP: Gå tilbake til standardvisning ved å klikke 'tilbake' (eller lukk dette vinduet om det er nytt)
****


