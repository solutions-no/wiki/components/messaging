= Fellesløsninger ref. Torget vår 2019)
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: main@messaging:messaging-appendixes:
endif::[]
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 0



.Fellesløsninger ref. Torget vår 2019)
image::{imagepath}Fellesløsninger ref. Torget vår 2019).png[alt=Fellesløsninger ref. Torget vår 2019) image, link=https://altinn.github.io/ark/models/archi-all?view=fd126def-ebaa-480d-ba45-87894d42d9a6]


TIP: Gå tilbake til standardvisning ved å klikke 'tilbake' (eller lukk dette vinduet om det er nytt)


[cols ="1,3", options="header"]
.Elementer i view for Fellesløsninger ref. Torget vår 2019)
|===

| Element
| Beskrivelse

| BR-digitalisering
a| 

| Digital Eiendomshandel
a| 

| Altinn styring av tilgang
a| Med denne løsningen styrer du hvem som kan bruke en digital tjeneste. Du kan bruke den til å styre tilganger for privatpersoner og til å gi tilganger basert på rettigheter en person har på vegne av en virksomhet.

| Altinn varsling
a| Vil du varsle innbyggere, virksomheter og organisasjoner på SMS eller e-post, kan du bruke løsningen Varsling. Brukeren logger seg deretter inn for å lese informasjonen du har sendt. 

| Altinn sending av dokument
a| Denne løsningen kan du bruke til å sende og motta dokumenter og store datamengder. Du bruker en sikker infrastruktur til formidling og kan spore forsendelsene.

| Altinn Informasjonsportal
a| 

| Digital kontaktinformasjon og fullmakter for virksomheter (KoFuVi
a| 

| Altinn samtykke
a| Altinn samtykke gir juridisk samhandlingsevne ved brukerstyrt samtykke. Innbygger samtykker til at data som er avgitt til én offentlig myndighet kan deles med andre aktører, også private. Samtykke brukes når virksomheten ikke har hjemmel til å innhente opplysningene.

Altinn samtykke muliggjør teknisk samhandlingsevne ved at datatilbyder, etter et samtykke fra sluttbruker, kan autorisere datakonsument og overføre aktuelle data.

Mer om Altinn samtykke:
https://www.altinndigital.no/produkter/samtykke/
https://altinn.github.io/docs/utviklingsguider/samtykke/

| Samtykkebasert lånesøknad
a| 

| Altinn digital post til virksomheter
a| Skal du sende sikker digital post til næringsdrivende og andre virksomheter med organisasjonsnummer, bruker du denne løsningen.

| Altinn API
a| Med Altinn API kan du lage løsninger som fungerer utenfor Altinn, men samtidig benytte Altinn sine tekniske komponenter. Altinn API snakker med systemene dine og du har kontroll på brukeropplevelsen. 

| Altinn digital dialog
a| Gjennom bruk av skjema legger løsningen til rette for brukerdialog med innbyggere og næringsliv på en sikker måte.

| Altinn hente data fra register
a| Med løsningen kan du gi brukerne dine tilgang til egne data som du har lagret i saksbehandlingssystem eller saksarkiv. 

| Altinn eBevis
a| eBevis forenkler utveksling av informasjon mellom leverandører og det offentlige i anskaffelsesprosessene. Den lar innkjøpere hente inn og gi tilgang til offentlig informasjon om leverandørerer før og etter kontraktsinngåelse.

| Altinn Tjenesteutviklingsløsning (TUL)
a| Altinn TUL gir deg tilgang til et sett med verktøy du behøver for å utvikle tjenester på Altinn-plattformen.

| Kartverket
a| 

| Matrikkelen
a| Matrikkelen er Norges offisielle register over fast eiendom, og under dette bygninger, boliger og adresser. Informasjon du behøver i planlegging, utbygging, bruk av og vern av fast eiendom samt i kjøp og salg av bolig. 

| KS
a| 

| KS FIKS tjenesteplattform
a| KS FIKS Tjenesteplattform er en plattform for utvikling og drift av kommunale applikasjonstjenester. 

| KS SvarUt/SvarInn
a| Med løsningen kan du formidle dokumenter fra kommunale saksarkiv og fagsystemer til innbyggere, næringsliv, offentlige og private organ på en sikker måte. SvarUt benyttes for utgående post, mens SvarInn benyttes for innkommende post. 

| BR
a| 

| Enhetsregisteret
a| I Enhetsregisteret finner du samtlige norske foretak, så vel statlige som private. Alle er identifisert med et nisifret organisasjonsnummer. Registeret samordner opplysninger fra MVA-registeret, Konkursregisteret med flere.

| BR-digitalisering (Infoforvaltning)
a| 

| Felles Begrepskatalog
a| 

| Felles Datamodellkatalog
a| 

| Felles API-katalog
a| Del av Felles datakatalog som gir mulighet for å søke etter API-er og lese API-spesifikasjoner https://fellesdatakatalog.brreg.no/apis

| Felles Datasettkatalog
a| 

| Felles Datakatalog
a| Felles Datakatalog gir en oversikt over hvilke data de ulike offentlige virksomhetene har, hvordan de henger sammen og hva de betyr. Videre tilbyr den en registeringsløsning for datasett, begreper og API.

Oversikt over datasett øker organisatorisk og juridisk samhandlingsevne ved at datasett som finnes tilgjengeliggjøres og juridiske aspekter ved datasettene beskrives.

Oversikt over informasjonsmodeller øker organisatorisk og semantisk samhandlingsevne ved at
informasjonsmodeller tilgjengeliggjøres og at data som utveksles beskrives med informasjonsmodeller.

Oversikt over begreper øker semantisk samhandlingsevne ved at felles begrepsbruk og
definisjoner er lett tilgjengelige.

Oversikt over APIer øker teknisk samhandlingsevne og muliggjør maskinell utveksling av data.

Mer om Felles datakatalog:
https://data.norge.no/

| Difi
a| Direktoratet for forvaltning og IKT (Difi) er det sentrale fagorganet for modernisering og omstilling av offentlig sektor. Difis fagområder er organisering, ledelse og arbeidsgiverstøtte, innovasjon, anskaffelser og digitalisering i offentlig sektor. For en nærmere omtale av Difis virkeområder og faste oppgaver, se virksomhets- og økonomiinstruksen.

Difi skal være den sentrale kilden til kunnskap om tilstand, utvikling og endringsbehov i forvaltningen på sine virkeområder. Difi skal også være orientert om den internasjonale utviklingen på disse områdene, og legge internasjonalt samarbeid til grunn for tiltak der dette er aktuelt.

Statlige virksomheter er målgruppen for alle fagområdene til direktoratet. I tillegg er kommunene målgruppe for blant annet offentlige innkjøp, innovasjon og digitalisering.

Difi skal være en faglig premissgiver, pådriver og støttespiller for den nødvendige omstillingen og innovasjonen i offentlig sektor gjennom å bruke ulike virkemidler, blant annet finansieringsordninger, digitalisering, ledelses- og kompetanseutviklingstiltak, anskaffelser og rådgivning på alle Difis fagområder.

Offentlig sektor skal være bærekraftig, effektiv og sette brukeren i sentrum.. Den enkelte virksomheten har ansvar for å oppnå dette målet ved å drive kontinuerlig forbedring og innovasjon knyttet til organisering, tjenester, produkter, arbeidsprosesser og kommunikasjonsformer. 


| eSignering
a| eSignering gir juridisk samhandlingsevne ved at virksomheter får sikker, praktisk og effektiv håndtering av dokumenter som krever signatur fra innbyggere.

Den digitale prosessen ved eSignering gjør at virksomhetene slipper manuell håndtering ved
utlevering og innhenting av dokumenter som skal signeres. Ved eSignering av dokumenter gjennomføres selve signeringen med elektronisk ID.

Mer om eSignering:
https://www.digdir.no/digitale-felleslosninger/esignering/789
https://samarbeid.difi.no/felleslosninger/esignering/dokumentasjon

| Elektronisk mottakerregister (ELMA)
a| ELMA er et register som inneholder aksesspunktadresser til virksomhetenes og hvilke
dokumenttyper, som elektroniske fakturaer, ordre, kataloger m.fl. mottakersystemet kan ta imot.

Aksesspunktet formidler meldinger til og fra andre aksesspunkter og til og fra avsender/mottaker maskin til maskin. Dette gir teknisk og organisatorisk samhandlingsevene.

Mer om ELMA:
https://www.digdir.no/digitale-felleslosninger/elektronisk-mottakerregisterelma/784

| eInnsyn​
a| eInnsyn gir juridisk samhandlingsevne ved at innbyggere kan søke og be om innsyn i dokumenter/postjournaler fra alle statlige, kommunale og fylkeskommunale organ som er publisert med eInnsyn.

Mer om eInnsyn:
https://www.digdir.no/digitale-felleslosninger/einnsyn/783
https://samarbeid.difi.no/felleslosninger/einnsyn/einnsyn-ny-rettleiar-innhaldsleverandorar-til-einnsyn

| Digital postkasse til innbyggere (DPI)
a| Digital postkasse gir teknisk samhandlingsevne ved at virksomheter kan sende digital post sikkert til innbyggerne, uavhengig av om innbygger har valgt e-Boks eller Digipost for mottak.

Mer om Digital postkasse til innbygger:
https://www.digdir.no/digitale-felleslosninger/digital-postkasse-tilinnbyggere/
775

| Kontakt- og reservasjonsregisteret​ for innbyggere (KRR)
a| KRR inneholder oversikt over mobilnummer og e-post, hvilken digital postkasse og eventuelle reservasjoner mot digital kommunikasjon innbyggerne i Norge har. Dette gir teknisk og  organisatorisk samhandlingsevne ved at virksomheter kan sende post og varslinger digitalt.

Mer om Kontakt- og reservasjonsregisteret:
https://www.digdir.no/digitale-felleslosninger/kontakt-ogreservasjonsregisteret-krr/865

| ID-porten
a| ID-porten gir teknisk samhandlingsevne den gjør det mulig for brukere å logge seg inn på offentlige tjenester med elektronisk ID.

Mer om ID-porten:
https://www.digdir.no/digitale-felleslosninger/id-porten/864

| Maskinporten
a| Maskinporten sørger for sikker autentisering og tilgangskontroll for datautveksling mellom
virksomheter, maskin til maskin. Dette gir teknisk og organisatorisk samhandlingsevne.

Mer om Maskinporten:
https://www.digdir.no/digitale-felleslosninger/maskinporten/869

| eFormidling
a| eFormidling er en løsning for sikker og effektiv meldingsutveksling i offentlig sektor. Hovedkomponentene i løsningen er et integrasjonspunkt som installeres lokalt hos
virksomhetene og et adresseregister som driftes av digitaliseringsdirektoratet. eFormidling gjør det mulig å kommunisere på en enkel og sikker måte, uten å måtte ta hensyn til om mottakeren er privat eller offentlig virksomhet, eller innbygger. Dette gir teknisk og organisatorisk samhandlingsevne.

Mer om eFormidling:
https://www.digdir.no/digitale-felleslosninger/eformidling/782

| Skatteetaten
a| 

| Folkeregisteret​
a| I Folkeregisteret henter du oppdatert nøkkelinformasjon om alle personer som er eller har vært bosatt i Norge: navn, adresse, kjønn, sivilstand, med mer. 

|===
****
TIP: Gå tilbake til standardvisning ved å klikke 'tilbake' (eller lukk dette vinduet om det er nytt)
****


