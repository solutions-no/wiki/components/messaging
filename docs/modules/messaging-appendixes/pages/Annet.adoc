= Annet
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: main@messaging:messaging-appendixes:
endif::[]
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 0



:leveloffset: +1
include::main@messaging:messaging-appendixes:page$Felles økosystem (Kartverkets framstilling).adoc[]
include::main@messaging:messaging-appendixes:page$Metadatakatalog RA (Erik, Riksrevisjonen).adoc[]
:leveloffset: -1
