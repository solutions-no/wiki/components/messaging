= Promoteheus-X Building blocks (2022)
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: main@messaging:messaging-appendixes:
endif::[]
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 0



.Promoteheus-X Building blocks (2022)
image::{imagepath}Promoteheus-X Building blocks (2022).png[alt=Promoteheus-X Building blocks (2022) image, link=https://altinn.github.io/ark/models/archi-all?view=id-498990adba0d4bcbb55429d5c1b199f1]


TIP: Gå tilbake til standardvisning ved å klikke 'tilbake' (eller lukk dette vinduet om det er nytt)


[cols ="1,3", options="header"]
.Elementer i view for Promoteheus-X Building blocks (2022)
|===

| Element
| Beskrivelse

| Catalog
a| https://dataspace.prometheus-x.org/building-blocks/catalog

| Consent
a| https://dataspace.prometheus-x.org/building-blocks/consent

| Contract
a| https://dataspace.prometheus-x.org/building-blocks/contract

| Identity
a| https://dataspace.prometheus-x.org/building-blocks/identity

| Traceability
a| https://dataspace.prometheus-x.org/building-blocks/traceability

| Monitoring
a| https://dataspace.prometheus-x.org/building-blocks/monitoring

| Anonymization / Pseudonymization
a| https://dataspace.prometheus-x.org/building-blocks/anonymization-pseudonymization

| Interoperability
a| 

| Learning records
a| https://dataspace.prometheus-x.org/building-blocks/interoperability/learning-records

| Universal plugin SDK
a| https://dataspace.prometheus-x.org/building-blocks/interoperability/universal-plugin-sdk

| AI metadata enrichment
a| https://dataspace.prometheus-x.org/building-blocks/interoperability/ai-metadata-enrichment

| Skills data
a| https://dataspace.prometheus-x.org/building-blocks/interoperability/skills-data

| Impact study
a| https://dataspace.prometheus-x.org/building-blocks/interoperability/impact-study

| Digital identity interoperability across systems and countries
a| https://docs.google.com/document/d/1U3o6LN2PROjACF9xrNenPhK7xviDWBAgA39LbS7UuKs/edit

| Enabling decentralized AI in the data space
a| https://docs.google.com/document/d/1C9--NGnOmEmLQ5johEmUs7BdTbNndi5DHjR2WF-8DN4/edit

| Skills Analytics Dashboard
a| https://drive.google.com/drive/u/1/folders/1YC4vdD99vr6JOn1CEt__AJGcNXHus7aa

| Edge / Distributed AI consent and contract management
a| https://drive.google.com/drive/u/1/folders/1YC4vdD99vr6JOn1CEt__AJGcNXHus7aa

| Data value chain tracker
a| https://docs.google.com/document/d/1kShsQdcXLU80qbJcS0Si_M-c6h55bix-Nkph-nhaue0/edit

Name of the building block: 
Data value chain tracker

Organizations involved: 
IMC
VISIONS
Nomadslab

Coordinator: IMC
Current situation and Opportunity
The European skill dataspace aims to guarantee users and organizations data sovereignty. This means that in contrast to today´s closed digital platforms users and organizations may track and control the use of their personal or business data by other parties after making it available for sharing. It is expected that on one hand this lowers the barriers for users and organizations to share sensitive data since the data remains to be protected by dataspace rules, consent and business contracts. On the other hand it may enable the creation of new incentives and business models that stimulate the exploitation of available data. 
For example, an operator of an AI-based recommender service for skill development requires data about training opportunities from training institutions as well as actual development paths from individuals. To motivate individuals to share their development path data for improving the AI-based recommendation engine (using machine learning techniques) the operator could distribute some of the value it generates when the recommendation engine is applied to the individuals who contributed their data by offering them premium services or even participation in the generated revenue.
This type of data-based business scenarios are expected to create substantial economic leverage in the context of dataspaces that goes beyond the one of closed digital platforms. Therefore the infrastructure of the European skill dataspace should facilitate the implementation of data-based business scenarios where data providers participate in the value created by services using their data.

Solution hypothesis
Considering that the European Skills dataspace infrastructure will provide consent and contract tracking and management functions for individuals as well as for all organizations participating in a data value chain we assume that the implementation of participative data-driven business models as described above can be supported by a number of technical functions operating on the consent and contract tracking (meta-)data associated with the data value chain. These functions can be provided by the European Skills dataspace as a module to be integrated by service providers that wish to implement a participative data-driven business model, alternatively, as a SaaS solution provided by the federation operating the European Skills dataspace or by a participant of the dataspace.

Project goal (this building block)
In this project we propose to 
identify first use cases that can leverage this type of business model, e.g. in the context of AI-based training and career recommendations, AI-supported content creation or similar,
develop the concept and the functions to facilitate the implementation of participative data-driven business models based on the consent and contract tracking infrastructure in the European Skills dataspace,
adapt and augment the building blocks of the consent and contract tracking infrastructure if necessary,
implement first participative data-driven business model(s) using the defined concept and functions as a module or as a service,
augment the templates for European Skills dataspace-conform contracts and consent to cover the first participative data-driven business model(s)
test, iterate until identified use case(s) are covered

Describe the standards you will rely on:

Describe how the roles between partners are divided and how each mobilize its expertise:


| Data veracity assurance
a| https://drive.google.com/drive/u/1/folders/1YC4vdD99vr6JOn1CEt__AJGcNXHus7aa

| Distributed skills data visualization service
a| https://docs.google.com/document/d/1UKjQMJKlsTKKzZuvHBZ2KL9HtjAlSzzn7vwVkUWETgo/edit

| Edge AI Translators
a| https://docs.google.com/document/d/1OQIcch7t4JQ1x4I0XVTYbDtyAQfoN2l6KI50J87CpOE/edit

|===
****
TIP: Gå tilbake til standardvisning ved å klikke 'tilbake' (eller lukk dette vinduet om det er nytt)
****


