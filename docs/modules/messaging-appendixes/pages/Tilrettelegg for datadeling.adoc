= Tilrettelegg for datadeling
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: main@messaging:messaging-appendixes:
endif::[]
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 0



.Tilrettelegg for datadeling
image::{imagepath}Tilrettelegg for datadeling.png[alt=Tilrettelegg for datadeling image, link=https://altinn.github.io/ark/models/archi-all?view=fc8c4a98-f2a0-4795-8c54-c0a4fbddf814]


****
xref:main@messaging:messaging-appendixes:page$Tilrettelegg for datadeling.var.1.adoc[Vis detaljer om elementene i diagrammet] (Tips: kbd:[Shift]-klikk for å åpne i nytt vindu)
****


