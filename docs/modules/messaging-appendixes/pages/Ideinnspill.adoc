= Ideinnspill
:wysiwig_editing: 1
ifeval::[{wysiwig_editing} == 1]
:imagepath: ../images/
endif::[]
ifeval::[{wysiwig_editing} == 0]
:imagepath: main@messaging:messaging-appendixes:
endif::[]
:experimental:
:toclevels: 4
:sectnums:
:sectnumlevels: 0

...

:leveloffset: +1
include::main@messaging:messaging-appendixes:page$Eriks tidligere innspill.adoc[]
:leveloffset: -1
